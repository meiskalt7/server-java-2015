

import labs.Warrior;
import labs.Worker;

import java.io.DataInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

/**
 * @author Eiskalt on 17.09.2015.
 */
class ServerConnectionProcessor extends Thread {
    private Socket sock;
    static HashMap<Integer, Vector<Worker>> clientsWork = new HashMap<>();
    static HashMap<Integer, Vector<Warrior>> clientsWar = new HashMap<>();
    private static ArrayList<Integer> clientList = new ArrayList<>();
    private int connectedClient, senderClient;//������������ ������, ������ � �������� ����������
    DataInputStream inStream;
    ObjectInputStream ois;
    ObjectOutputStream oos;

    public ServerConnectionProcessor(Socket s) {
        sock = s;
        try {
            inStream = new DataInputStream(
                    sock.getInputStream());
            oos = new ObjectOutputStream(sock.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            /*
            2)	TCP-������ ������ ������� ����������� �������� � �������� ����� ������������� ������� ������
            ��� ������������.+ ���������� ����� ������ ��������� � ��������� �������, ��� ����� � ������ ����������
            ����������� ������ ���� ������������ � ������� ��������.+ ��� ������������ �������� ��� ��� �����������
            ����� ������ ������ �����������;
             */
            int op = inStream.readInt();
            switch (op) {
                case 0:
                    sendClientList();
                    break;
                case 1:
                    addClient();
                    break;
                case 2:
                    sendAnts();
                    break;
                case 3:
                    removeClient();
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addClient() {
        try {
            connectedClient = inStream.readInt();// ����� ������������� �������
            //���� ��� ����, �� ���������
            if (!clientList.contains(connectedClient))
                clientList.add(connectedClient); //��������� ��� � ����


            ois = new ObjectInputStream(sock.getInputStream());
            System.out.println("ois+");
            Vector<Worker> workers = (Vector<Worker>) ois.readObject();
            Vector<Warrior> warriors = (Vector<Warrior>) ois.readObject();

            //������� ��� ���� ���� ����� � �������
            clientsWork.remove(connectedClient);
            clientsWar.remove(connectedClient);

            //��������� ������� ���������
            clientsWork.put(connectedClient, workers);
            clientsWar.put(connectedClient, warriors);


            oos.writeObject(clientList);
            oos.close();
            System.out.println("client connected");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendAnts() {
        try {
            System.out.println("sendAnts");
            inStream = new DataInputStream(sock.getInputStream());
            senderClient = inStream.readInt();
            int n;
            n = inStream.readInt();
            System.out.println("Sending list " + senderClient);
            System.out.println("Number " + n);
            //���������� N ��������
            Vector<Worker> tempWork = new Vector<>();
            Vector<Warrior> tempWar = new Vector<>();
            for(int i = 0; i <= n; i++) {
                tempWork.add(clientsWork.get(senderClient).get(i));
                tempWar.add(clientsWar.get(senderClient).get(i));
            }
            oos.writeObject(tempWork);
            oos.writeObject(tempWar);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendClientList() {
        try {
            System.out.println("Connecting...");
            connectedClient = inStream.readInt();// ����� ������������� �������
            System.out.println(connectedClient);
            //���� ��� ����, �� ���������
            if (!clientList.contains(connectedClient))
                clientList.add(connectedClient); //��������� ��� � ����
            oos.writeObject(clientList);
            oos.close();
            System.out.println("clientList updated");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeClient() {
        try {
            connectedClient = inStream.readInt();// ����� ������������� �������
            //���� ��� ����, �� ���������
            if (clientList.contains(connectedClient))
                clientList.remove((Object)connectedClient);

            //������� ��� ���� ���� ����� � �������
            clientsWork.remove(connectedClient);
            clientsWar.remove(connectedClient);

            oos.close();
            System.out.println("client deleted");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
