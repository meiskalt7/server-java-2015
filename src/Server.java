import java.lang.Exception;
import java.lang.String;
import java.lang.System;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author Eiskalt on 17.09.2015.
 */
public class Server {
    public static void main(String[] args) {

        try {
            System.out.println("Server is running");
            int port = 3333;
            // �������� ���������� ������
            ServerSocket ss = new ServerSocket(port);
            // ���� �������� � ��� ������� ������� ��������� �����
            while (true) {
                Socket s = ss.accept();
                s.setSoTimeout(0);
                ServerConnectionProcessor p = new ServerConnectionProcessor(s);
                p.start();
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

}
